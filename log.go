package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
	"sync"
)

var (
	std            = logrus.StandardLogger()
	callerInitOnce sync.Once

	// My package name cache
	myPackage          string
	minimumCallerDepth = 10
	maximumCallerDepth = 25

	pathSeparator = string(os.PathSeparator)
)

func init() {
	//DecorateLogger(log)
	logrus.SetReportCaller(true)
	logrus.SetLevel(logrus.DebugLevel)

	std.Formatter = &logrus.TextFormatter{
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			if caller := getCaller(); caller != nil {
				pathComponents := strings.Split(caller.File, pathSeparator)
				filename := pathComponents[len(pathComponents)-1]
				return fmt.Sprintf("%-50s()", getFunctionName(caller.Function)),
					fmt.Sprintf("%-15s", fmt.Sprintf("%s:%d", filename, caller.Line))
			}
			return "NF", "NA"
		},
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05.000",
	}
}

// Return function name part of function
func getFunctionName(f string) string {
	idx := strings.LastIndex(f, pathSeparator)
	if idx != -1 {
		return f[idx+1:]
	}
	return f
}

// getPackageName reduces a fully qualified function name to the package name
// There really ought to be to be a better way...
func getPackageName(f string) string {
	for {
		lastPeriod := strings.LastIndex(f, ".")
		lastSlash := strings.LastIndex(f, "/")
		if lastPeriod > lastSlash {
			f = f[:lastPeriod]
		} else {
			break
		}
	}

	return f
}

// getCaller retrieves the name of the first non-logrus calling function
func getCaller() *runtime.Frame {

	// cache this package's fully-qualified name
	callerInitOnce.Do(func() {
		pcs := make([]uintptr, 2)
		_ = runtime.Callers(0, pcs)
		//fmt.Println("****myFuncForPC: ", runtime.FuncForPC(pcs[1]).Name())
		myPackage = getPackageName(runtime.FuncForPC(pcs[1]).Name())
		//fmt.Println("$$$$myPackage:", myPackage)
	})

	// Restrict the lookback frames to avoid runaway lookups
	pcs := make([]uintptr, maximumCallerDepth)
	depth := runtime.Callers(minimumCallerDepth, pcs)
	frames := runtime.CallersFrames(pcs[:depth])

	iamFound := false

	for f, again := frames.Next(); again; f, again = frames.Next() {
		pkg := getPackageName(f.Function)

		//fmt.Println("######myCallers:Function:", f.Function)

		// If found my name, next one supposed to be the true caller

		if iamFound {
			//fmt.Println("FunCFound: ", &f)
			return &f
		}

		if pkg == myPackage {
			iamFound = true
		}
	}

	// if we got here, we failed to find the caller's context
	return nil
}
